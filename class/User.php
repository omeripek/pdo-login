<?php
class User {
    private $_db,
            $_data;
    public function __construct($user = null){
        $this->_db = DB::getInstance();
    }

    public function create($fields = array()) {
        if(!$this->_db->insert('kullanici', $fields)){
            throw new Exception('Hesap oluşturmada sorun oluştu!');
        }
     }

    public function find($user = null) {
        if($user) {
            // if user had a numeric username this FAILS...
            $field = (is_numeric($user)) ? 'id' : 'username';
            $data = $this->_db->get('kullanini', array($field, '=', $user));

            if($data->count()) {
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }

        public function login($username = null, $password = null) {
            $user = $this->find($username);
            print_r($user);
            return false;

        }

}