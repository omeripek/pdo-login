<?php
include_once ('conf/db.php');
class dbc {
    public $host;
    public $user;
    public $pass;
    public $db;
    public function __construct($host,$db,$user,$pass) {
        try {
            $this->dbc = new PDO("mysql:host=$host;dbname=$db;charset=utf8",$user,$pass);
            $this->dbc->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbc->query('SET NAMES utf8');
            $this->dbc->query('SET CHARACTER SET utf8');
        } catch(PDOException $e) {
            $this->error = 'ERROR: ' . $e->getMessage();
            return $this->error;
        }
    }

}


/*  ----------------------------------------------------  Sil/Taşı ------------------------------------------------------------------------- */
/* Parametreler ile */
class makalelerA {
    var $kategoriA;
    var $baslikA;
    var $yazarA;
    var $yayin_tarihiA;
        function __construct($kategoriA,$baslikA,$yazarA,$yayin_tarihiA) {
            $this -> kategoriA = $kategoriA;
            $this -> baslikA = $baslikA;
            $this -> yazarA = $yazarA;
            $this -> yayin_tarihiA = $yayin_tarihiA;
        }
}

/* Parametreler ile */
class makaleler {
    var $kategori;
    var $baslik;
    private $yazar;
    public  $yayin_tarihi;
    function yeniMethod($kategori, $baslik, $yazar, $yayin_tarihi){
        $this -> kategori = $kategori;
        $this -> baslik = $baslik;
        $this -> yazar = $yazar;
        $this -> yayin_tarihi =$yayin_tarihi;
    }

    function setYazar($yazar){
        $this -> yazar = $yazar;
    }

    function getYazar() {
        return $this -> yazar;
    }

}

/* Parametresiz */
class prmYk {
    function __construct(){
        echo "Nesneyi oluşturduk";
    }
}


class uni {
    var $adi = "A üniversitesi";
    var $mekan = "Web";
}

class fakulte extends uni {
    var $bolumadi = "Yazılım Müh";
    function info() {
        echo "Fakülte sınıfındayız..."."<br />";
    }
}
class ogrenci extends fakulte{
    var $ogrenci_kimligi;
    function  __construct($ogrenci_kimligi){
        $this ->ogrenci_kimligi = $ogrenci_kimligi;
    }
}