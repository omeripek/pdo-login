<?php
class Validate{
    private $_passed = false,
            $_errors = array(),
            $_db = null;
    public function __construct() {
        $this->_db = DB::getInstance();
    }
    public function check($source, $items = array()){
        foreach($items as $item => $rules) {
            foreach($rules as $rule => $rule_value){
               // echo "{$item} {$rule} şu şekilde olmalı: {$rule_value}<br /> "; Aldığı değerleri gördük
                $value = trim($source[$item]);
                $item = escape($item);
               // echo $value.'<br />';
                if($rule === 'required' && empty($value)){
                    $this->addError("{$item} zorunludur!");
                }else if(!empty($value)) {
                    switch($rule) {
                        case 'min':
                            if(strlen($value) < $rule_value){
                                $this->addError("{$item} en az {$rule_value} karakter olmalı");
                            }

                        break;
                        case 'max':
                            if(strlen($value) > $rule_value){
                                $this->addError("{$item} en fazla {$rule_value} karakter olmalı");
                            }
                            break;
                        case 'matches':
                            if($value != $source[$rule_value]){
                                $this->addError("{$rule_value} ile {$item}  eşleşmedi. ");
                            }
                            break;
                        case 'unique':
                             $this->_db->get($rule_value, array($item, '=', $value));
                             if($this->_db->count()){
                                    $this->addError("Böyle bir {$item} zaten var!");
                             }

                        // **********************************************************
                          /*  $check = DB::getInstance();
                            $check->get('kullanici',array($item,'=',$value));
                            if (!$check->Count()) {
                                echo 'Kullanıcı var';
                            } else {
                                echo 'User exists';
                            }*/
                        // **********************************************************

                            break;
                    }
                }
            }
        }
            if(empty($this->_errors)){
               $this->_passed = true;
            }
            return $this;
        }

    private function addError($error){
        $this->_errors[] = $error;
    }
    public function errors(){
        return $this->_errors;
    }
    public function passed(){
        return $this->_passed;
    }
}
