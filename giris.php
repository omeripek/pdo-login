<?php
require_once 'core/init.php';

if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
           'username' => array('required' => true),
            'sifre' => array('required' => true)
        ));

        if($validation->passed()){
            $user = new User();
            $giris = $user->login(Input::get('username'), Input::get('sifre'));

            if($giris){
                echo 'Giris Yaptiniz, Lutfen cikiniz!';
            }else {
                echo 'Site uyelik bitmis!';
            }

        }else {
            foreach($validation->errors() as $error){
                echo $error, '<br />';
            }
        }
    }
}
?>
<form action="" method="POST">
    <div class="field">
        <label for="username">Kullanici Adi: </label>
        <input type="text" name="username" id="username" autocomplete="off" />
    </div>
    <div class="field">
        <label for="sifre">Sifre: </label>
        <input type="password" name="sifre" id="sifre" autocomplete="off" />
    </div>
    <input type="hidden" name="token" value="<?php echo Token::generate();?>"/>
    <input type="submit" value="Giris Yap"/>
</form>