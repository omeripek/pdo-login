<?php
require_once 'core/init.php';
date_default_timezone_set('Europe/Istanbul');

if(Input::exists()){
    if(Token::check(Input::get('token'))){
          // echo $_POST['username']; bunun yerine
            // echo Input::get('password'); input değerini yazdır
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'username' => array(
                  'required' => true,
                  'min' => 2,
                  'max' => 25,
                  'unique' => 'kullanici'
                ),
                'password' => array(
                    'required' => true,
                    'min' => 6
                ),
                'password_again' => array(
                    'required' => true,
                    'matches' => 'password'
                ),
                'adsoy' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 50
                )
            ));

            if($validate->passed()){
                $user = new User();
                $salt = Hash::salt(32);
                try {
                    $user->create(array(
                       'username' => Input::get('username'),
                       'sifre' => Hash::make(Input::get('sifre'), $salt),
                       'adsoy' => Input::get('adsoy'),
                       'slt' => $salt,
                       'sonaktif' => date('Y-m-d H:i:s'),
                       'grup' => 1
                    ));

                    Session::flash('anasayfa', 'Üyeliğiniz gerçekleşmiştir, giriş yapabilirsiniz.');
                   // header('Location: index.php');
                    //Redirect::to(404);
                    Redirect::to('index.php');

                } catch(Exception $e) {
                    die($e->getMessage());
                }
               // Session::flash('success','Üyelik Başarılı');
              //  header('Location: index.php');
              //  echo "Başarılı";
              //  var_dump($validation);
              //  echo "<br />";
              //  print_r($validation);

            }else {
                //print_r($validation->errors());
                foreach($validate->errors() as $error){
                    echo $error, '<br />';
                }
            }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Üye Kayıt</title>
</head>
<body>
<form action="" method="POST">
    <div class="field">
        <label for="username">Kullanıcı Adı:</label>
        <input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off"/>
    </div>
    <div class="field">
        <label for="password">Şifre:</label>
        <input type="password" name="password" id="password" value=""/>
    </div>
    <div class="field">
        <label for="password_again">Şifreyi Tekrar Yaz:</label>
        <input type="password" name="password_again" id="password_again" value=""/>
    </div>
    <div class="field">
        <label for="name">İsim:</label>
        <input type="text" name="adsoy" id="name" value="<?php echo escape(Input::get('adsoy')); ?>">
    </div>
    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
    <input type="submit" value="Üye Ol" />
</form>
</body>
</html>